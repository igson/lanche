package org.am.lanche.testing;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.jayway.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

import org.am.lanche.config.AppConfig;
import org.am.lanche.model.Pedido;
import org.am.lanche.model.Produto;
import org.am.lanche.model.Promocao;
import org.am.lanche.model.StatusPedido;
import org.am.lanche.model.TipoProduto;
import org.am.lanche.model.dto.PedidoDto;
import org.am.lanche.model.dto.ProdutoDto;
import org.am.lanche.utils.LancheUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jayway.restassured.response.Response;

@ContextConfiguration(classes={AppConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class ProdutoTeste {

	private final String URL = "http://localhost:8080/lanche"; 
	
	
	//@Test
	public void cadastraNovoProduto() {
		
		ProdutoDto xtudo = new ProdutoDto();
				xtudo.setCodigo("006");
				xtudo.setDescontoUnitario(new BigDecimal(1.50));
				xtudo.setPreco(new BigDecimal(10));
				xtudo.setNome("X-Tudo Carne Sadia");
				xtudo.setTipoProduto(TipoProduto.SANDUICHE);
	
				Response response = given()
						   .contentType("application/json")
						   .body(xtudo)
						   .when()
					       .post(URL + "/produto");

				assertEquals(response.getStatusCode() , 201);

				System.out.println("Produto: " + response.body().asString() );

				ProdutoDto buscaXTudo = given()
						   .header("Accept", "application/json")
						   .contentType("application/json")
					       .pathParam("codigo", xtudo.getCodigo() )
					       .get(URL + "/produto/{codigo}")
						   .andReturn()
						   .as(ProdutoDto.class);		
				
				assertEquals( xtudo.getCodigo() , buscaXTudo.getCodigo());
				
	}
	
	//@Test
	public void listarTodosProdutos() throws ParseException {
			
			Response response = given()
					   .contentType("application/json")
					   .when()
				       .get(URL + "/produto/listar");
			
			List<ProdutoDto> objetos = Arrays.asList(response.as(ProdutoDto[].class));
			
			List<Produto> produtos = objetos.stream().map( obj -> LancheUtils.fromProduto(obj) ).collect(Collectors.toList() );

			for (Produto pedido : produtos) {
				System.out.println(pedido.toString());
			}
			
			assertEquals(6, produtos.size());
			
			
	}
	
	
	//@Test
	public void listarPorCategoriaSanduiche() throws ParseException {
			
			Response response = given()
					   .contentType("application/json")
					   .pathParam("categoria", TipoProduto.SANDUICHE )
					   .when()
				       .get(URL + "/produto/{categoria}/listar");
			
			List<ProdutoDto> objetos = Arrays.asList(response.as(ProdutoDto[].class));
			
			List<Produto> produtos = objetos.stream().map( obj -> LancheUtils.fromProduto(obj) ).collect(Collectors.toList() );

			for (Produto pedido : produtos) {
				System.out.println(pedido.toString());
			}
			
			assertEquals(3, produtos.size());
			
			
		}
		
	@Test
	public void removerProduto() throws ParseException {
			
		Response response = given()
				   .contentType("application/json")
				   .pathParam("codigo", "006" )
				   .when()
			       .delete(URL + "/produto/{codigo}");
		

		assertEquals(response.getStatusCode() , 204);
		
	}
	
}
