package org.am.lanche.testing;

import static com.jayway.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.am.lanche.config.AppConfig;
import org.am.lanche.model.ItemPedido;
import org.am.lanche.model.Produto;
import org.am.lanche.model.dto.PedidoDto;
import org.am.lanche.model.dto.ProdutoDto;
import org.am.lanche.utils.LancheUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jayway.restassured.response.Response;


@ContextConfiguration(classes={AppConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class AlterarPedidoTeste {

	
		private final String URL = "http://localhost:8080/lanche"; 
	
	
		@Test
		public void alterarPedido() {
			
			PedidoDto pedidoResgatadoDto = given()
						.header("Accept", "application/json")
					   .contentType("application/json")
					   .pathParam("codigo", "P001" )
					   .when()
				       .get(URL + "/pedido/{codigo}").as(PedidoDto.class);
			
			ProdutoDto cocaCola1LDto = given()
					   .header("Accept", "application/json")
				       .pathParam("codigo", "004")
				       .get(URL + "/produto/{codigo}")
					   .andReturn()
					   .as(ProdutoDto.class);
			
			ProdutoDto xsaladaDto = given()
					   .header("Accept", "application/json")
				       .pathParam("codigo", "001")
				       .get(URL + "/produto/{codigo}")
					   .andReturn()
					   .as(ProdutoDto.class);
			
			ProdutoDto pizzaGrandeDto = given()
					   .header("Accept", "application/json")
				       .pathParam("codigo", "005")
				       .get(URL + "/produto/{codigo}")
					   .andReturn()
					   .as(ProdutoDto.class);
			
			Produto cocaCola1Lt = LancheUtils.fromProduto(cocaCola1LDto);
			
			Produto xsalada = LancheUtils.fromProduto(xsaladaDto);
			
			Produto pizzaGrande = LancheUtils.fromProduto(pizzaGrandeDto);
			
			ItemPedido itemCocaCola1Lt = new ItemPedido();
					   itemCocaCola1Lt.setProduto(cocaCola1Lt);
					   itemCocaCola1Lt.setQuantidade(1);
					   itemCocaCola1Lt.setOperacao('-');
					   
			ItemPedido itemPizzaGrande = new ItemPedido();
					   itemPizzaGrande.setProduto(pizzaGrande);
					   itemPizzaGrande.setQuantidade(1);
					   itemPizzaGrande.setOperacao('+');	
					   
			ItemPedido itemXsalada = new ItemPedido();
					   itemXsalada.setProduto(xsalada);
					   itemXsalada.setQuantidade(2);
					   itemXsalada.setOperacao('-');
					   
		    List<ItemPedido> novos = new ArrayList<ItemPedido>();
		  				 	 novos.add(itemPizzaGrande);
		  				 	 //novos.add(itemCocaCola1Lt);
		  				 	 //novos.add(itemXsalada);
					   
			pedidoResgatadoDto.setItens(novos);
			 
			Response response = given()
					   .contentType("application/json")
					   .pathParam("codigo", pedidoResgatadoDto.getCodigo())
					   .body(pedidoResgatadoDto)
					   .when()
				       .put(URL + "/pedido/{codigo}/alterar");
			
			assertEquals(200, response.getStatusCode() );
			
		}
		
		
	
}
