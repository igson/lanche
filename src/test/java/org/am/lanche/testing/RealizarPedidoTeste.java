package org.am.lanche.testing;

import static com.jayway.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.am.lanche.config.AppConfig;
import org.am.lanche.iservice.IPedidoService;
import org.am.lanche.iservice.IProdutoService;
import org.am.lanche.model.ItemPedido;
import org.am.lanche.model.Pedido;
import org.am.lanche.model.Produto;
import org.am.lanche.model.Promocao;
import org.am.lanche.model.StatusPedido;
import org.am.lanche.model.dto.PedidoDto;
import org.am.lanche.model.dto.ProdutoDto;
import org.am.lanche.utils.LancheUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jayway.restassured.response.Response;
import com.sun.mail.imap.protocol.Item;

import freemarker.core.ArithmeticEngine.BigDecimalEngine;



@ContextConfiguration(classes={AppConfig.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class RealizarPedidoTeste {

	private static final Logger LOG = LogManager.getLogger("TEST-LOG:");
	
	private final String URL = "http://localhost:8080/lanche"; 
	

	@Test
	public void realizarPedidoTeste() {
		
		 ProdutoDto xsalada = given()
			   .header("Accept", "application/json")
		       .pathParam("codigo", "001")
		       .get(URL + "/produto/{codigo}")
			   .andReturn()
			   .as(ProdutoDto.class);
		 
		 ProdutoDto xbacon = given()
				   .header("Accept", "application/json")
			       .pathParam("codigo", "002")
			       .get(URL + "/produto/{codigo}")
				   .andReturn()
				   .as(ProdutoDto.class);
		 
		 ProdutoDto fanta = given()
				   .header("Accept", "application/json")
			       .pathParam("codigo", "003")
			       .get(URL + "/produto/{codigo}")
				   .andReturn()
				   .as(ProdutoDto.class);
		 
		 ProdutoDto pizza = given()
				   .header("Accept", "application/json")
			       .pathParam("codigo", "005")
			       .get(URL + "/produto/{codigo}")
				   .andReturn()
				   .as(ProdutoDto.class);
		 
		 assertEquals("001", xsalada.getCodigo());
		 assertEquals("002", xbacon.getCodigo());
		 assertEquals("003", fanta.getCodigo());
		 
		 ZoneId zone2 = ZoneId.of("Brazil/East");
		 
		 PedidoDto pedidoItalo = new PedidoDto();
			 	   pedidoItalo.setData( LocalDate.now() );
			 	   pedidoItalo.setStatusPedido(StatusPedido.PENDENTE);
			 	   pedidoItalo.setHora( LocalTime.now(zone2) );
			 	   pedidoItalo.setCodigo("P001");
		 	    
		 Produto p1 = LancheUtils.fromProduto(xsalada);
		 Produto p2 = LancheUtils.fromProduto(xbacon);
		 Produto p3 = LancheUtils.fromProduto(fanta);
		 Produto p4 = LancheUtils.fromProduto(pizza);
		 
		 ItemPedido itemXsalada = new ItemPedido();
		 			itemXsalada.setProduto(p1);
		 			itemXsalada.setQuantidade(2);
		 			
		 ItemPedido itemXbacon = new ItemPedido();
		 			itemXbacon.setProduto(p2);
		 			itemXbacon.setQuantidade(2);
		 
		 ItemPedido itemFanta = new ItemPedido();
		 			itemFanta.setProduto(p3);
		 			itemFanta.setQuantidade(2);
		 
		 			
		 List<ItemPedido> itens = new ArrayList<ItemPedido>();			
		            itens.add(itemXsalada);
		            itens.add(itemXbacon);
		            itens.add(itemFanta);
		 
		            pedidoItalo.setItens(itens);
		            
		            
		 Response response = given()
		   .contentType("application/json")
		   .pathParam("promocao", Promocao.SEM_DESCONTO)
		   .body(pedidoItalo)
		   .when()
	       .post(URL + "/pedido/{promocao}/registrar");
		 
		assertEquals(response.getStatusCode() , 201);
		 
	}
	
	
	//@Test
	public void listarPedidosFechados() throws ParseException {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Response response = given()
				   .header("Accept", "application/json")
				   .contentType("application/json")
				   .parameter("status", StatusPedido.REALIZADO)
				   .parameter("dataEntregaPedido", sdf.format( sdf.parse("2018-08-05") ) )
				   .when()
			       .get(URL + "/pedido/list");
		
		List<PedidoDto> objetos = Arrays.asList(response.as(PedidoDto[].class));
		
		List<Pedido> pedidos = objetos.stream().map( obj -> new Pedido(obj)).collect(Collectors.toList() );

		for (Pedido pedido : pedidos) {
			System.out.println(pedido.toString());
		}
		
		assertEquals(3, pedidos.size());
		
	}
	
	
	
	//@Test
	public void valorTotalVendasAoDia() throws ParseException {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Response response = given()
					.header("Accept", "application/json")
				   .contentType("application/json")
				   .pathParam("data", sdf.format( sdf.parse(LocalDate.now().toString()) ) )
				   .when()
			       .get(URL + "/pedido/valor/total/{data}");
		
		BigDecimal valotTotal = response.getBody().as(BigDecimal.class);
		
		System.out.println("Valor Total: " + valotTotal);
		
		assertEquals(200, response.getStatusCode());
		
	}
	
	
	
	
}
