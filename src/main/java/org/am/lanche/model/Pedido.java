package org.am.lanche.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.am.lanche.model.dto.PedidoDto;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonIgnore;

/*@NamedNativeQueries({ 
	@NamedNativeQuery(name = "Conta.buscarContaCidadaoSQLNativoPorCpf", query = " SELECT DISTINCT c.id_conta, c.des_nome, c.des_email, c.num_cpf, c.enum_tipo_usuario from tbl_conta as c INNER join tbl_perfil_conta as pc on ( pc.id_conta = c.id_conta ) INNER join tbl_perfil as p on ( p.id_perfil = pc.id_perfil ) where c.num_cpf = :cpf and c.des_senha = md5(:senha) and p.id_sistema = :sistemaId ", resultSetMapping = "ContaMapping"),
	@NamedNativeQuery(name = "Conta.buscarContaCidadaoSQLNativoPorEmail", query = " SELECT DISTINCT c.id_conta, c.des_nome, c.des_email, c.num_cpf, c.enum_tipo_usuario from tbl_conta as c INNER join tbl_perfil_conta as pc on ( pc.id_conta = c.id_conta ) INNER join tbl_perfil as p on ( p.id_perfil = pc.id_perfil ) where c.des_email = :email and c.des_senha = md5(:senha) and p.id_sistema = :sistemaId ", resultSetMapping = "ContaMapping")
})

@SqlResultSetMappings({
	@SqlResultSetMapping(
			name="ContaMapping",
	classes={ 
			@ConstructorResult(
					targetClass=Pedido.class,
					columns={
				  		 @ColumnResult(name = "id_conta", type=Integer.class),
				  		 @ColumnResult(name = "des_nome", type=String.class),
				  		 @ColumnResult(name = "des_email", type=String.class),
				  		 @ColumnResult(name = "num_cpf", type=String.class),
				  		 @ColumnResult(name = "enum_tipo_usuario", type=String.class)
					}
			)
	})
})*/



@Entity
@Audited
@Table(name = "tbl_pedido", schema = "lanche")
@AuditTable(value = "tbl_pedido_auditoria", schema = "auditoria")
@SequenceGenerator(name = "tbl_pedido_id_seq", sequenceName = "TBL_PEDIDO_ID_SEQ", allocationSize=1, schema="lanche")
public class Pedido implements Serializable {

	private static final long serialVersionUID = -3132477233873927462L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tbl_pedido_id_seq")
	private Integer id;
	
	@Column(name = "codigo_pedido", nullable=false)
	private String codigo;

	@Enumerated(EnumType.STRING)
	@Column(name = "status_pedido", nullable = false)
	private StatusPedido statusPedido;

	@Column(name = "data_pedido", nullable = false)
	private LocalDate data;
	
	@Column(name = "hora")
	private LocalTime hora;
	
	@Audited
	@Column(name = "vlr_total", nullable = false)
	private BigDecimal valorTotal = new BigDecimal(0.0);
	
	@Audited
	@JsonIgnore
	@OneToMany(mappedBy = "pedido", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private List<ItemPedido> itens = new ArrayList<ItemPedido>();

	
	public Pedido() {}
	
	
	public Pedido(PedidoDto pedidoDto) {
		this.id = pedidoDto.getId();
		this.statusPedido = pedidoDto.getStatusPedido();
		this.data = pedidoDto.getData();
		this.hora = pedidoDto.getHora();
		this.valorTotal = pedidoDto.getValorTotal();
		this.codigo = pedidoDto.getCodigo();
	}
	
	
	@Autowired
	public void adicionarItem(ItemPedido itenNovo) {
		
		if ( this.itens.size() == 0 ) {
		
			this.itens.add(itenNovo);
			itenNovo.setPedido(this);
		
		} else {
			
			Optional<ItemPedido> buscaItem = this.itens.stream().filter( p -> p.getProduto().getCodigo().equals( itenNovo.getProduto().getCodigo() ) ).findAny();
			
			boolean itemEncontrado = buscaItem.isPresent();
			
			if ( itemEncontrado == false ) {
				System.out.println("A1 --- Adicionando " + itenNovo.getProduto().getNome() + " à lista.");
				this.itens.add(itenNovo);
				itenNovo.setPedido(this);
			
			} else {
				
				System.out.println("Quantidade Item " + itenNovo.getProduto().getNome()  + " : " +  itenNovo.getQuantidade() + " Antigo: " + buscaItem.get().getQuantidade() );
				
				if ( buscaItem.get().getQuantidade() > 0 ) {
						
					System.out.println("Atualizando quantidade " + itenNovo.getProduto().getNome() + " à lista.");
					
					itens.stream().map( temp -> { 
							
							if ( temp.getProduto().getCodigo().equals( itenNovo.getProduto().getCodigo() ) ) {
								
								if ( itenNovo.getOperacao().equals('+') ) {
									temp.setQuantidade( temp.getQuantidade() + itenNovo.getQuantidade() );	
								} else {
									temp.setQuantidade( temp.getQuantidade() - itenNovo.getQuantidade() );
								}
								
							}		
								return temp;
						} 
				
					).collect( Collectors.toList() );
				
				} 
				
			}

		}
		
	}

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public StatusPedido getStatusPedido() {
		return statusPedido;
	}


	public void setStatusPedido(StatusPedido statusPedido) {
		this.statusPedido = statusPedido;
	}


	

	public LocalDate getData() {
		return data;
	}


	public void setData(LocalDate data) {
		this.data = data;
	}


	public LocalTime getHora() {
		return hora;
	}


	public void setHora(LocalTime hora) {
		this.hora = hora;
	}


	public BigDecimal getValorTotal() {
		return valorTotal;
	}


	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}


	public List<ItemPedido> getItens() {
		return itens;
	}


	public void setItens(List<ItemPedido> itens) {
		this.itens = itens;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pedido other = (Pedido) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Pedido [id=" + id + ", codigo=" + codigo + ", statusPedido=" + statusPedido + ", data="
				+ data + ", hora=" + hora + ", valorTotal=" + valorTotal + "]";
	}

	
	
}
