package org.am.lanche.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Audited
@Table(name="tbl_item_pedido", schema="lanche")
@AuditTable(value="tbl_item_pedido_auditoria", schema="auditoria")
@SequenceGenerator(name="tbl_item_pedido_id_seq", sequenceName="tbl_item_pedido_id_seq", allocationSize=1, schema="lanche")
public class ItemPedido implements Serializable {

	private static final long serialVersionUID = -2941333939003387437L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tbl_item_pedido_id_seq")
	private Integer id;
	
	@Column(name = "valor")
	private BigDecimal preco = new BigDecimal(0);
	
	@Column(name = "quantidade", nullable=false)
	private Integer quantidade = new Integer(0);
	
	@ManyToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name = "pedido_id")
	private Pedido pedido = new Pedido();

	@OneToOne(fetch=FetchType.LAZY)
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
	private Produto produto = new Produto();
	
	@Transient
	private Character operacao;
	
	
	public ItemPedido() {
		// TODO Auto-generated constructor stub
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getPreco() {
		return preco;
	}

	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Character getOperacao() {
		return operacao;
	}

	public void setOperacao(Character operacao) {
		this.operacao = operacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((preco == null) ? 0 : preco.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemPedido other = (ItemPedido) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (preco == null) {
			if (other.preco != null)
				return false;
		} else if (!preco.equals(other.preco))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ItemPedido [id=" + id + ", preco=" + preco+ ", pedido=" + pedido + ", produto=" + produto + "]";
	}
	
	
	
}
