package org.am.lanche.model;

public enum TipoProduto {

	SANDUICHE, REFRIGERANTE, DOCE, SUCO, MASSAS, OUTROS;
	
}
