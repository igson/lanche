package org.am.lanche.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.am.lanche.model.ItemPedido;
import org.am.lanche.model.Pedido;
import org.am.lanche.model.StatusPedido;


public class PedidoDto implements Serializable {

	private static final long serialVersionUID = -4664831276962198095L;

	private Integer id;

	private StatusPedido statusPedido;

	private LocalDate data;

	private LocalTime hora;
	
	private BigDecimal valorTotal = new BigDecimal(0.0);
	
	private String codigo;
	
	private List<ItemPedido> itens = new ArrayList<ItemPedido>();

	
	public PedidoDto() {}
	
	
	public PedidoDto(Pedido pedido) {
		this.id = pedido.getId();
		this.statusPedido = pedido.getStatusPedido();
		this.data = pedido.getData();
		this.hora = pedido.getHora();
		this.valorTotal = pedido.getValorTotal();
		this.codigo = pedido.getCodigo();
		if ( !pedido.getItens().isEmpty() ) {
			this.itens = pedido.getItens();	
		}
	}
	
	public void adicionarItem(ItemPedido itenNovo) {
		this.itens.add(itenNovo);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public StatusPedido getStatusPedido() {
		return statusPedido;
	}

	public void setStatusPedido(StatusPedido statusPedido) {
		this.statusPedido = statusPedido;
	}

	

	public LocalDate getData() {
		return data;
	}


	public void setData(LocalDate data) {
		this.data = data;
	}


	public LocalTime getHora() {
		return hora;
	}


	public void setHora(LocalTime hora) {
		this.hora = hora;
	}


	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public List<ItemPedido> getItens() {
		return itens;
	}

	public void setItens(List<ItemPedido> itens) {
		this.itens = itens;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	@Override
	public String toString() {
		return "PedidoDto [id=" + id + ", statusPedido=" + statusPedido + ", hora=" + hora
				+ ", data=" + data + ", valorTotal=" + valorTotal + ", codigo=" + codigo + "]";
	}
	
}
