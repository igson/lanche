package org.am.lanche.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import org.am.lanche.model.Produto;
import org.am.lanche.model.TipoProduto;

public class ProdutoDto implements Serializable {
	
	private static final long serialVersionUID = 7841151299607296617L;

	private Integer id;
	
	private String codigo;
	
	private String nome;
	
	private BigDecimal preco;
	
	private TipoProduto tipoProduto;
	
	private BigDecimal descontoUnitario;

	public ProdutoDto() {}
	
	
	public ProdutoDto(Produto produto) {
		this.id = produto.getId();
		this.codigo = produto.getCodigo();
		this.nome = produto.getNome();
		this.preco = produto.getPreco();
		this.tipoProduto = produto.getTipoProduto();
		this.descontoUnitario = produto.getDescontoUnitario();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getPreco() {
		return preco;
	}

	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}

	public TipoProduto getTipoProduto() {
		return tipoProduto;
	}

	public void setTipoProduto(TipoProduto tipoProduto) {
		this.tipoProduto = tipoProduto;
	}

	public BigDecimal getDescontoUnitario() {
		return descontoUnitario;
	}

	public void setDescontoUnitario(BigDecimal descontoUnitario) {
		this.descontoUnitario = descontoUnitario;
	}


	@Override
	public String toString() {
		return "ProdutoDto [id=" + id + ", codigo=" + codigo + ", nome=" + nome + ", preco=" + preco + ", tipoProduto="
				+ tipoProduto + ", descontoUnitario=" + descontoUnitario + "]";
	}
	
	
}
