package org.am.lanche.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "tbl_combo_produto", schema = "lanche")
@SequenceGenerator(name = "tbl_combo_produto_seq", sequenceName = "tbl_combo_produto_seq", allocationSize=1, schema="lanche")
public class ComboProduto {

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tbl_combo_produto_seq")
	private Integer id;
	
	@Column(name = "nome", nullable = false, length=100)
	private String nome;
	
	@Column(name = "preco", nullable = false)
	private BigDecimal preco;

	
	
}
