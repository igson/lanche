package org.am.lanche.model;

public enum Promocao {

	SEM_DESCONTO("SEM DESCONTO"), DIA_DOS_NAMORADOS("DIA DOS NAMORADOS"), 
	NATAL("NATAL"), QUEIMA_ESTOQUE("QUEIMA DE ESTOQUE"), DESCONTO_PROGRESSIVO("DESCONTO PROGRESSIVO"), 
	DESCONTO_PRODUTO_UNITARIO("DESCONTO POR PRODUTO UNITÁRIO");
	
	private String descricao;
	
	Promocao(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
        return descricao;
    }
	
}
