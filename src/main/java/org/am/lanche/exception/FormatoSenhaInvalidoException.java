package org.am.lanche.exception;

public class FormatoSenhaInvalidoException extends Exception {


	private static final long serialVersionUID = 5553796094428863895L;

	
	public FormatoSenhaInvalidoException(String mensagem) {
		super(mensagem);
	}
	
	
}
