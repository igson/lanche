package org.am.lanche.exception;

public class MaisDeUmaContaException  extends Exception {

	
	private static final long serialVersionUID = -5819605964534503262L;

	public MaisDeUmaContaException( String mensagem ) {
		super(mensagem);
	}
	

}