package org.am.lanche.exception;

public class EmailValidationException extends RuntimeException {

	private static final long serialVersionUID = -3481375546418941029L;

	public EmailValidationException( String msg ) {
		super(msg);
	}
	
	public EmailValidationException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
	
}
