package org.am.lanche.exception;

public class CPFInvalidoException extends RuntimeException {

	private static final long serialVersionUID = -3481375546418941029L;

	public CPFInvalidoException( String msg ) {
		super(msg);
	}
	
	public CPFInvalidoException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
	
}
