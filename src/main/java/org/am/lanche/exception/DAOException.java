package org.am.lanche.exception;

import java.io.Serializable;

public class DAOException extends Exception implements Serializable {

	private static final long serialVersionUID = -253453171363770638L;

	public DAOException(String message) {
		super(message);
	}

	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}
}