package org.am.lanche.exception;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;



@ControllerAdvice
public class ResourceExceptionHandler {
	
	private SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");    
	
	private long dts = System.currentTimeMillis(); 
	
	@ExceptionHandler(SemItensException.class)
	public ResponseEntity<StandardError> negocioException(SemItensException e, HttpServletRequest request) {
		StandardError err = new StandardError( sdf.format( new Date(dts) ), HttpStatus.NOT_FOUND.value(), "Não encontrado." , e.getMessage(), request.getRequestURI() );
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(err);
	}
	
	@ExceptionHandler(CPFInvalidoException.class)
	public ResponseEntity<StandardError> negocioException(CPFInvalidoException e, HttpServletRequest request) {
		StandardError err = new StandardError(sdf.format( new Date(dts) ), HttpStatus.BAD_REQUEST.value(), "Regra de Negócio", e.getMessage(), request.getRequestURI());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
	}
	
	@ExceptionHandler(ExpirideTokenException.class)
	public ResponseEntity<StandardError> negocioException(ExpirideTokenException e, HttpServletRequest request) {
		StandardError err = new StandardError(sdf.format( new Date(dts) ), HttpStatus.NON_AUTHORITATIVE_INFORMATION.value(), "Erro", e.getMessage(), request.getRequestURI());
		return ResponseEntity.status(HttpStatus.NON_AUTHORITATIVE_INFORMATION).body(err);
	}
	
	@ExceptionHandler(EmailValidationException.class)
	public ResponseEntity<StandardError> negocioException(EmailValidationException e, HttpServletRequest request) {
		StandardError err = new StandardError(sdf.format( new Date(dts) ), HttpStatus.BAD_REQUEST.value(), "Regra de Negócio", e.getMessage(), request.getRequestURI());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
	}
	
	@ExceptionHandler(ContaInativaException.class)
	public ResponseEntity<StandardError> negocioException(ContaInativaException e, HttpServletRequest request) {
		StandardError err = new StandardError(sdf.format( new Date(dts) ), HttpStatus.UNAUTHORIZED.value(), "Autorização", e.getMessage(), request.getRequestURI() );
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(err);
	}
	
	@ExceptionHandler(NegocioException.class)
	public ResponseEntity<StandardError> negocioException(NegocioException e, HttpServletRequest request) {
		StandardError err = new StandardError(sdf.format( new Date(dts) ), HttpStatus.BAD_REQUEST.value(), "Regra de Negócio", e.getMessage(), request.getRequestURI());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
	}
	
	@ExceptionHandler(NotFoundException.class)
	public ResponseEntity<StandardError> negocioException(NotFoundException e, HttpServletRequest request) {
		StandardError err = new StandardError(sdf.format( new Date(dts) ), HttpStatus.NOT_FOUND.value(), "Regra de Negócio", e.getMessage(), request.getRequestURI());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(err);
	}
	
	
	@ExceptionHandler(UsuarioOuSenhaInvalidosException.class)
	public ResponseEntity<StandardError> negocioException(UsuarioOuSenhaInvalidosException e, HttpServletRequest request) {
		StandardError err = new StandardError(sdf.format( new Date(dts) ), HttpStatus.UNAUTHORIZED.value(), "Autorização", e.getMessage(), request.getRequestURI());
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(err);
	}

	@ExceptionHandler(DataIntegrityException.class)
	public ResponseEntity<StandardError> dataIntegrity(DataIntegrityException e, HttpServletRequest request) {
		
		StandardError err = new StandardError(sdf.format( new Date(dts) ), HttpStatus.BAD_REQUEST.value(), "Integridade de dados", e.getMessage(), request.getRequestURI());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<StandardError> validation(MethodArgumentNotValidException e, HttpServletRequest request) {
		
		ValidationError err = new ValidationError( sdf.format( new Date(dts) ), HttpStatus.UNPROCESSABLE_ENTITY.value(), "Erro de validação", e.getMessage(), request.getRequestURI() );
		for (FieldError x : e.getBindingResult().getFieldErrors()) {
			err.addError(x.getField(), x.getDefaultMessage());
		}		
		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(err);
	}

	@ExceptionHandler(AuthorizationException.class)
	public ResponseEntity<StandardError> authorization(AuthorizationException e, HttpServletRequest request) {
		
		StandardError err = new StandardError(sdf.format( new Date(dts) ), HttpStatus.FORBIDDEN.value(), "Acesso negado", e.getMessage(), request.getRequestURI());
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(err);
	}

	@ExceptionHandler(FileException.class)
	public ResponseEntity<StandardError> file(FileException e, HttpServletRequest request) {
		
		StandardError err = new StandardError(sdf.format( new Date(dts) ), HttpStatus.BAD_REQUEST.value(), "Erro de arquivo", e.getMessage(), request.getRequestURI());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
	}

	
}