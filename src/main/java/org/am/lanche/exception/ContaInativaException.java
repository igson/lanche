package org.am.lanche.exception;

public class ContaInativaException extends RuntimeException {

	private static final long serialVersionUID = -3481375546418941029L;

	public ContaInativaException( String msg ) {
		super(msg);
	}
	
	public ContaInativaException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
	
}
