package org.am.lanche.exception;

public class ContaExistenteException  extends Exception {

	
	private static final long serialVersionUID = 2882351016817169725L;

	
	public ContaExistenteException( String mensagem ) {
		super(mensagem);
	}
	

}