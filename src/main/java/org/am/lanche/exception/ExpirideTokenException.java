package org.am.lanche.exception;

public class ExpirideTokenException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public ExpirideTokenException(String msg) {
		super(msg);
	}
	
	public ExpirideTokenException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
