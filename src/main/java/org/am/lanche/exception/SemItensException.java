package org.am.lanche.exception;

public class SemItensException extends Exception {

	private static final long serialVersionUID = -3481375546418941029L;

	public SemItensException() {
		super("Esta conta não possui perfil de acesso a este sistema!");
	}

}
