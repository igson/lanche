package org.am.lanche.exception;

public class ContaSemUgAtivaException extends Exception {

	private static final long serialVersionUID = -5532516175965443245L;

	public ContaSemUgAtivaException() {
		super("Sua conta não possui vínculo ativo a nenhuma unidade gestora!");
	}

}
