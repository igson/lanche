package org.am.lanche.exception;

public class UsuarioOuSenhaInvalidosException extends RuntimeException {

	private static final long serialVersionUID = -3481375546418941029L;

	public UsuarioOuSenhaInvalidosException( String msg ) {
		super(msg);
	}
	
	public UsuarioOuSenhaInvalidosException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
	
}
