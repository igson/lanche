package org.am.lanche.resources;

import java.io.Serializable;
import java.net.URI;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.am.lanche.iservice.IProdutoService;
import org.am.lanche.model.Pedido;
import org.am.lanche.model.Produto;
import org.am.lanche.model.Promocao;
import org.am.lanche.model.StatusPedido;
import org.am.lanche.model.TipoProduto;
import org.am.lanche.model.dto.PedidoDto;
import org.am.lanche.model.dto.ProdutoDto;
import org.am.lanche.utils.LancheUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


@RestController
@RequestMapping(value="/produto")
public class ProdutoResource implements Serializable {

	private static final long serialVersionUID = 6332907190625252668L;

	
	@Autowired private IProdutoService produtoService;
	
	private static final Logger LOG = LogManager.getLogger("PRODUTO-LOG:");
	 
	 
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Void> registrarPedido(@RequestBody ProdutoDto objetoDto) {
		
		System.out.println("Produto: " + objetoDto.toString());
		
		Produto produto = LancheUtils.fromProduto(objetoDto);
		
		produto = this.produtoService.salvar(produto); 
		
		objetoDto = LancheUtils.fromProdutoDTO(produto);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/codigo").buildAndExpand( objetoDto.getCodigo() ).toUri();

		return ResponseEntity.created(uri).build();
		
	}
	
	
	@RequestMapping(value="/listar", method=RequestMethod.GET)
	public ResponseEntity<List<ProdutoDto>> listarTodos() {
		
		List<Produto> produtos = this.produtoService.listarTodos();
		
		List<ProdutoDto> objetos = produtos.stream().map( obj -> new ProdutoDto(obj)).collect(Collectors.toList() );
		
		return ResponseEntity.ok().body(objetos);
		
	}
	
	
	@RequestMapping(value="/{categoria}/listar", method=RequestMethod.GET)
	public ResponseEntity<List<ProdutoDto>> listarPorCategoria(@PathVariable("categoria") TipoProduto tipoProduto) {
		
		List<Produto> produtos = this.produtoService.listarPorCategoria(tipoProduto);
		
		List<ProdutoDto> objetos = produtos.stream().map( obj -> new ProdutoDto(obj)).collect(Collectors.toList() );
		
		return ResponseEntity.ok().body(objetos);
		
	}
	
	
	
	@RequestMapping(value="/{codigo}", method=RequestMethod.GET)
	public ResponseEntity<ProdutoDto> buscarPorCodigo(@PathVariable("codigo") String codigo) {
		
		Produto produto = this.produtoService.buscaPorCodigo(codigo);
		
		ProdutoDto objeto = LancheUtils.fromProdutoDTO(produto);
		
		return new ResponseEntity<ProdutoDto>(objeto, HttpStatus.ACCEPTED);
		
	}
	
	
	@RequestMapping(value="/{codigo}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> removerPorCodigo(@PathVariable("codigo") String codigo) {
		System.out.println("Código Informado? " + codigo );
		this.produtoService.remover(codigo);
		return ResponseEntity.noContent().build();
	}
	
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> removerPorId(@PathVariable("id") Integer id) {
		this.produtoService.remover(id);
		return ResponseEntity.noContent().build();
	}
	
	
	
}
