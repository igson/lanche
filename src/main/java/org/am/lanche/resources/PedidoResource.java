package org.am.lanche.resources;

import java.math.BigDecimal;
import java.net.URI;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.websocket.server.PathParam;

import org.am.lanche.iservice.IPedidoService;
import org.am.lanche.model.ItemPedido;
import org.am.lanche.model.Pedido;
import org.am.lanche.model.Promocao;
import org.am.lanche.model.StatusPedido;
import org.am.lanche.model.dto.PedidoDto;
import org.am.lanche.utils.LancheUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.assertj.core.data.Percentage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;



@RestController
@RequestMapping(value="/pedido")
public class PedidoResource {
	
	
	@Autowired private IPedidoService pedidoService;

	private static final Logger LOG = LogManager.getLogger("PEDIDO-LOG:");
	

	
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public ResponseEntity<List<PedidoDto>> listarPedidos(@RequestParam(value="status") StatusPedido statusPedido,
														  @RequestParam(value="dataEntregaPedido") @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate dataEntregaPedido) {
		
		List<Pedido> pedidos = this.pedidoService.listar(statusPedido, dataEntregaPedido);
		
		List<PedidoDto> objetos = pedidos.stream().map( obj -> new PedidoDto(obj)).collect(Collectors.toList() );
		
		return ResponseEntity.ok().body(objetos);
		
	}
	
	
	
	@RequestMapping(value="/{promocao}/registrar", method=RequestMethod.POST)
	public ResponseEntity<Void> registrarPedido(@RequestBody PedidoDto pedidoDto, @PathVariable("promocao") Promocao promocao) {
		
		System.out.println("Tamanho da Lista: " + pedidoDto.getItens().size() );
		
		Pedido pedido = LancheUtils.fromPedido(pedidoDto);
		
		pedido = pedidoService.salvar(pedido, promocao, pedidoDto.getItens() );
		
		URI uri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/codigo").buildAndExpand( pedido.getCodigo() ).toUri();

		return ResponseEntity.created(uri).build();
		
	}
	
	
	@RequestMapping(value="/{codigo}/alterar", method=RequestMethod.PUT)
	public ResponseEntity<Void> alterarPedido(@RequestBody PedidoDto pedidoDto, @PathVariable("codigo") String codigo) {
		
		Pedido pedidoResgatado = this.pedidoService.buscaPorCodigo(codigo);
		
		List<ItemPedido> itens = pedidoDto.getItens();
		
		this.pedidoService.atualizar(pedidoResgatado, itens);

		return ResponseEntity.ok().build();
		
	}
	
	
	@RequestMapping(value="/{codigo}/adicionar", method=RequestMethod.PUT)
	public ResponseEntity<Void> adicionarItemPedido(@RequestBody PedidoDto pedidoDto, @PathVariable("codigo") String codigo) {
		
		Pedido pedidoResgatado = this.pedidoService.buscaPorCodigo(codigo);
		
		List<ItemPedido> itens = pedidoDto.getItens();
		
		this.pedidoService.atualizar(pedidoResgatado, itens);

		return ResponseEntity.ok().build();
		
	}
	
	
	
	@RequestMapping(value="/valor/total/{data}", method=RequestMethod.GET)
	public ResponseEntity<BigDecimal> listarPedidos(@PathVariable(value="data") 
													 @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate data) {
		
		BigDecimal valorTotal = this.pedidoService.valorTotalVendaNoDia(data);
		
		return new ResponseEntity<BigDecimal>(valorTotal, HttpStatus.OK);
		
	}
	
	
	@RequestMapping(value="/{codigo}/pagamento", method=RequestMethod.PUT)
	public ResponseEntity<Void> realizarPagamentoPedido(@PathVariable("codigo") String codigo) {
		
		this.pedidoService.realizarPagamento(codigo);

		return ResponseEntity.ok().build();
		
	}
	
	
	
	@RequestMapping(value="/{codigo}", method=RequestMethod.GET)
	public ResponseEntity<PedidoDto> buscarPorCodigo(@PathVariable("codigo") String codigo) {
		
		Pedido pedido = this.pedidoService.buscaPorCodigo(codigo);
		
		PedidoDto objeto = LancheUtils.fromPedidoDTO(pedido);
		
		LOG.warn(objeto.toString());
		
		return new ResponseEntity<PedidoDto>(objeto, HttpStatus.ACCEPTED);
		
	}
	
	
}
