package org.am.lanche.iservice;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.am.lanche.model.ItemPedido;
import org.am.lanche.model.Pedido;
import org.am.lanche.model.Promocao;
import org.am.lanche.model.StatusPedido;

public interface IPedidoService {
	
	public BigDecimal valorTotalVendaNoDia(LocalDate data);
	
	public Pedido buscaPorCodigo(String codigo);
	
	public Pedido salvar(Pedido pedido, Promocao promocao, List<ItemPedido> itens);
	
	public List<Pedido> listar(StatusPedido status);
	
	public void realizarPagamento(String codigo);
	
	public void atualizar(Pedido pedido, List<ItemPedido> novosItens);
	
	public List<Pedido> listar(StatusPedido statusPedido, LocalDate dataEntregaPedido);
	
}
