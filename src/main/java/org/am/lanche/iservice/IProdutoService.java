package org.am.lanche.iservice;

import java.util.List;

import org.am.lanche.model.Produto;
import org.am.lanche.model.TipoProduto;

public interface IProdutoService {
	
	public Produto salvar(Produto produto);
	public void remover(Integer id);
	public void remover(String codigo);
	public List<Produto> listarTodos();
	public Produto buscaPorCodigo(String codigo);
	public List<Produto> listarPorCategoria(TipoProduto tipoProduto);
	
}
