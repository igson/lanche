package org.am.lanche.config;

import java.net.MalformedURLException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.am.lanche.model.Promocao;
import org.am.lanche.model.dto.PedidoDto;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;


@Configuration
public class ClienteLancheApi {

	private  final Client client = ClientBuilder.newClient();

	private String maquina = "localhost";

	private String porta = "8080";

	private String appName = "lanche";

	private String protocolo = "http://";
	
	private final String BASE_URI_PEDIDO = protocolo + maquina + ":" + porta + "/" + appName + "/pedido";

	private Response response ;	
	
	 private static final Logger LOG = LogManager.getLogger("API-LOG:");
	
	
	public  void registarPedido(PedidoDto pedidoDto, Promocao promocao) throws MalformedURLException {
		
		try {
		
			response = client.target(BASE_URI_PEDIDO)
						     .queryParam("promocao", promocao )
						     .request(MediaType.APPLICATION_JSON)
							 .post(Entity.entity(pedidoDto, MediaType.APPLICATION_JSON));
						     

			if (response.getStatus() != HttpStatus.CREATED.value()) {
				System.out.println("ERROR: " + response.getStatus() );
			}
			
			
		} finally {
			response.close();
		}

	}
	

}
