package org.am.lanche.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = {"org.am.lanche.model"})
@EnableJpaRepositories(basePackages = {"org.am.lanche.repositories"})
@EnableTransactionManagement
@PropertySource(value = { "classpath:application.properties" })
public class SpringDataConfig {

	//private static final String PROPERTY_NAME_HIBERNATE_HBM2DDL_AUTO = "spring.jpa.hibernate.ddl-auto";
	//private static final String PROPERTY_NAME_HIBERNATE_DIALECT = "spring.jpa.database-platform";
	//private static final String PROPERTY_NAME_HIBERNATE_SHOW_SQL = "spring.jpa.show-sql";
	//private static final String PROPERTY_NAME_HIBERNATE_ENABLE_LAZY_LOAD_NO_TRANS = "spring.jpa.properties.hibernate.enable_lazy_load_no_trans";
	//private static final String PROPERTY_NAME_HIBERNATE_FORMAT_SQL = "spring.jpa.properties.hibernate.format_sql";
	
/*	
	@Autowired
	private Environment environment;

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory factory) throws IOException {

		JpaTransactionManager manager = new JpaTransactionManager();
							  manager.setEntityManagerFactory( entityManagerFactory().getObject() );
							//  manager.setJpaDialect(new HibernateJpaDialect());

		return manager;

	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws IOException {

		HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
		adapter.setShowSql(true);
		adapter.setGenerateDdl(true);

		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
						factory.setJpaVendorAdapter(adapter);
						factory.setDataSource(dataSource());
						factory.setPackagesToScan(new String[] { "org.am.lanche"});
						factory.setJpaProperties(hibernateProperties());
						
		return factory;

	}

	@Bean(name = "dataSource")
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
								dataSource.setDriverClassName(environment.getRequiredProperty("spring.datasource.driver-class-name"));
								dataSource.setUrl(environment.getRequiredProperty("spring.datasource.url"));
								dataSource.setUsername(environment.getRequiredProperty("spring.datasource.username"));
								dataSource.setPassword(environment.getRequiredProperty("spring.datasource.password"));

		return dataSource;
	}

	private Properties hibernateProperties() {
		Properties properties = new Properties();
		//properties.put(PROPERTY_NAME_HIBERNATE_DIALECT, environment.getRequiredProperty(PROPERTY_NAME_HIBERNATE_DIALECT));
		//properties.put(PROPERTY_NAME_HIBERNATE_SHOW_SQL, environment.getRequiredProperty(PROPERTY_NAME_HIBERNATE_SHOW_SQL));
		//properties.put(PROPERTY_NAME_HIBERNATE_HBM2DDL_AUTO,environment.getRequiredProperty(PROPERTY_NAME_HIBERNATE_HBM2DDL_AUTO));
		//properties.put(PROPERTY_NAME_HIBERNATE_ENABLE_LAZY_LOAD_NO_TRANS, environment.getRequiredProperty(PROPERTY_NAME_HIBERNATE_ENABLE_LAZY_LOAD_NO_TRANS));
		//properties.put(PROPERTY_NAME_HIBERNATE_FORMAT_SQL, environment.getRequiredProperty(PROPERTY_NAME_HIBERNATE_FORMAT_SQL));

		return properties;
	}
*/
}