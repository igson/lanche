package org.am.lanche.config;

import java.math.BigDecimal;
import java.util.Arrays;

import org.am.lanche.model.Produto;
import org.am.lanche.model.TipoProduto;
import org.am.lanche.repositories.IProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;


@EnableAutoConfiguration
@ComponentScan({"org.am.lanche"})
@EntityScan(basePackages= {"org.am.lanche"})
@SpringBootApplication(scanBasePackages={"org.am.lanche"})
public class AppConfig extends SpringBootServletInitializer implements CommandLineRunner {

	@Autowired
	private IProdutoRepository produtoRepository;
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(AppConfig.class);
	}

	
	public static void main(String[] args) {
		SpringApplication.run(AppConfig.class, args);
	}

	
	public void run(String... args) {
		
		Produto xsalada = new Produto();
				xsalada.setCodigo("001");
				xsalada.setDescontoUnitario(new BigDecimal(2.00));
				xsalada.setNome("X-Salada");
				xsalada.setPreco( new BigDecimal(5.00) );
				xsalada.setTipoProduto(TipoProduto.SANDUICHE);
		
		Produto xbacon = new Produto();
				xbacon.setCodigo("002");
				xbacon.setDescontoUnitario(new BigDecimal(2.00));
				xbacon.setNome("X-Bacon'");
				xbacon.setPreco( new BigDecimal(7.00) );
				xbacon.setTipoProduto(TipoProduto.SANDUICHE);
		
		Produto fanta = new Produto();
				fanta.setCodigo("003");
				fanta.setDescontoUnitario(new BigDecimal(1.50));
				fanta.setNome("Fanta 500 Ml");
				fanta.setPreco( new BigDecimal(3.50) );
				fanta.setTipoProduto(TipoProduto.REFRIGERANTE);
		
		Produto coca = new Produto();
				coca.setCodigo("004");
				coca.setDescontoUnitario(new BigDecimal(1.50));
				coca.setNome("Coca Colca 1 Lt.");
				coca.setPreco( new BigDecimal(5.00) );
				coca.setTipoProduto(TipoProduto.REFRIGERANTE);
		
		Produto pizzaGrande = new Produto();
				pizzaGrande.setCodigo("005");
				pizzaGrande.setDescontoUnitario(new BigDecimal(2.50));
				pizzaGrande.setNome("Pizza Grande");
				pizzaGrande.setPreco( new BigDecimal(10.00) );
				pizzaGrande.setTipoProduto(TipoProduto.MASSAS);
		
				//this.produtoRepository.saveAll( Arrays.asList(xsalada, xbacon, fanta, coca, pizzaGrande) );
		
	}
	
	/*@Bean
	public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {  
	    ObjectMapper mapper = new ObjectMapper();
	    mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
	    mapper.registerSubtypes(PedidoDto.class);
	    mapper.registerSubtypes(Produto.class);
	    mapper.registerSubtypes(ItemPedido.class);
	    MappingJackson2HttpMessageConverter converter =new MappingJackson2HttpMessageConverter(mapper);
	    return converter;
	} */
	
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}


	
}
