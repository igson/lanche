package org.am.lanche.utils;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.am.lanche.model.Pedido;
import org.am.lanche.model.Produto;
import org.am.lanche.model.StatusPedido;
import org.am.lanche.model.dto.PedidoDto;
import org.am.lanche.model.dto.ProdutoDto;


public class LancheUtils {

	
	public static Pedido fromPedido( PedidoDto objeto ) {
		return new Pedido(objeto);
	}
	
	
	public static PedidoDto fromPedidoDTO( Pedido pedido ) {
		return new PedidoDto(pedido);
	}
	
	
	public static ProdutoDto fromProdutoDTO( Produto produto ) {
		return new ProdutoDto(produto);
	}
	
	
	public static Produto fromProduto( ProdutoDto objeto ) {
		
		Produto produto = new Produto();
				produto.setCodigo(objeto.getCodigo());
				produto.setId(objeto.getId());
				produto.setNome(objeto.getNome());
				produto.setPreco(objeto.getPreco());
				produto.setTipoProduto(objeto.getTipoProduto());
				produto.setDescontoUnitario(objeto.getDescontoUnitario());
				
		return produto;		
		
	}
	
	
}
