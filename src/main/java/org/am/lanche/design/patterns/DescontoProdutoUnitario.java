package org.am.lanche.design.patterns;

import java.math.BigDecimal;
import java.util.List;

import org.am.lanche.model.ItemPedido;
import org.am.lanche.model.Pedido;
import org.am.lanche.model.Promocao;

public class DescontoProdutoUnitario implements Desconto {

	private Desconto proximo;

	public void setProximo(Desconto proximo) {
		this.proximo = proximo;
	}

	
	public void calcular(Pedido pedido, Promocao promocao) {
		
		BigDecimal valorTotal = new BigDecimal(0.0);
		
		if ( promocao == Promocao.DESCONTO_PRODUTO_UNITARIO ) {
		
			List<ItemPedido> itens = pedido.getItens();

			for (ItemPedido itemPedido : itens) {
				
					itemPedido.setPreco( itemPedido.getPreco().add( itemPedido.getProduto().getPreco()
															  .multiply( BigDecimal.valueOf( itemPedido.getQuantidade() ) )
															  .subtract( itemPedido.getProduto().getDescontoUnitario().multiply( BigDecimal.valueOf( itemPedido.getQuantidade() ) ) ) ) );
				
					valorTotal = valorTotal.add( itemPedido.getPreco() );
					
					pedido.setValorTotal(valorTotal);
					
					itemPedido.setPedido(pedido);
				
			}
			
			for (ItemPedido itemPedido : itens) {
				
				System.out.println(itemPedido.getQuantidade() + " X " + itemPedido.getProduto().getNome() + "  = " + itemPedido.getPreco() );
				
			}
			
			
		} else if ( proximo != null ) {
			proximo.calcular(pedido, promocao);	
		}
		
	}

}