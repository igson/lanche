package org.am.lanche.design.patterns;

import org.am.lanche.model.Pedido;
import org.am.lanche.model.Promocao;

public class CalculadoraDeDesconto {
	
	private Desconto desconto;
	
	public CalculadoraDeDesconto() {
		
		Desconto descontoProdutoUnitario = new DescontoProdutoUnitario();
		Desconto descontoProgressivo = new DescontoProgressivo();
		Desconto naoTemDesconto = new SemDesconto();
		
		this.setDesconto(descontoProdutoUnitario);
		
		descontoProdutoUnitario.setProximo(descontoProgressivo);
		descontoProgressivo.setProximo(naoTemDesconto);
		
	}
	
	public void calculaDesconto(Pedido pedido, Promocao promocao) { 
		this.desconto.calcular(pedido, promocao);
	}
	
	public Desconto getDesconto() {
		return desconto;
	}

	public void setDesconto(Desconto desconto) {
		this.desconto = desconto;
	}
	
	
}
