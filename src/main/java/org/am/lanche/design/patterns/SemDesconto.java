package org.am.lanche.design.patterns;

import java.math.BigDecimal;

import java.util.List;
import java.util.function.Predicate;

import org.am.lanche.model.ItemPedido;
import org.am.lanche.model.Pedido;
import org.am.lanche.model.Produto;
import org.am.lanche.model.Promocao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SemDesconto implements Desconto {

	private static final Logger LOG = LogManager.getLogger("SEM-DESCONTO-LOG:");
	
	
	public void setProximo(Desconto proximo) { }
	
	public void calcular(Pedido pedido, Promocao promocao) {

		if (promocao == Promocao.SEM_DESCONTO) {

			List<ItemPedido> itens = pedido.getItens();
			
			for (ItemPedido itemPedido : itens) {
				System.out.println("Produto: " + itemPedido.getProduto().getNome() + " Preço: " + itemPedido.getProduto().getPreco() );
				System.out.println("Quantidade: " + itemPedido.getQuantidade());
				itemPedido.setPreco( itemPedido.getPreco().add( itemPedido.getProduto().getPreco().multiply( BigDecimal.valueOf( itemPedido.getQuantidade() ) ) ) );
				System.out.println("Produto: " + itemPedido.getProduto().getNome() + " " +  itemPedido.getQuantidade() + "X" + itemPedido.getProduto().getPreco() + " = " + itemPedido.getPreco() );
				itemPedido.getPedido().setValorTotal( itemPedido.getPedido().getValorTotal().add( itemPedido.getPreco() ) );
				System.out.println("Vlr. Total Acumulado: " + itemPedido.getPedido().getValorTotal() );
			}

		}

	}
	
	/*public static void main(String[] args) {
		
		Produto xsalada = new Produto();
				xsalada.setCodigo("001");
				xsalada.setNome("X-Salada");
				
		Produto xbacon = new Produto();
				xbacon.setCodigo("002");
				xbacon.setNome("X-Bacon");
				
		Produto cocacola = new Produto();
				cocacola.setCodigo("003");
				cocacola.setNome("Coca Cola");
				
		ItemPedido item1 = new ItemPedido();
				   item1.setProduto(xsalada);
				   
		ItemPedido item2 = new ItemPedido();
				   item2.setProduto(xbacon);		   
		
		ItemPedido item3 = new ItemPedido();
				   item3.setProduto(cocacola);
				   
		Pedido pedido = new Pedido();
			   pedido.setCodigo("X0IZ");
			   pedido.adicionarItem(item1);
			   pedido.adicionarItem(item1);
		
		List<ItemPedido> itens = pedido.getItens();
		
		for (ItemPedido itemPedido : itens) {
			
			System.out.println("Qtd. Produto " + itemPedido.getProduto().getNome() + " : " + itemPedido.getQuantidade() );
			
		}
		
	}*/
	

}