package org.am.lanche.design.patterns;

import java.math.BigDecimal;
import java.util.List;

import org.am.lanche.model.ItemPedido;
import org.am.lanche.model.Pedido;
import org.am.lanche.model.Promocao;


public class DescontoProgressivo  implements Desconto {

	private Desconto proximo;

	public void setProximo(Desconto proximo) {
		this.proximo = proximo;
	}
	
	
	public void calcular(Pedido pedido, Promocao promocao) {
		
		if ( promocao == Promocao.DESCONTO_PROGRESSIVO ) {
		
			List<ItemPedido> itens = pedido.getItens();

			int numeroItens = itens.size();
			
			if ( numeroItens >= 2 ) {
				
				BigDecimal desconto = new BigDecimal(5.0);
				
				for (ItemPedido itemPedido : itens) { 
					itemPedido.setPreco( itemPedido.getProduto().getPreco().subtract( desconto ) );
				}
				
			} else if ( numeroItens == 1 ) {
				
				BigDecimal desconto = new BigDecimal(1.0);
				
				for (ItemPedido itemPedido : itens) { 
					itemPedido.setPreco( itemPedido.getProduto().getPreco().subtract( desconto ) );
				}
				
			}

		} else if ( proximo != null ) {
			proximo.calcular(pedido, promocao);	
		}

	}

}