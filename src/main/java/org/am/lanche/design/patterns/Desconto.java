package org.am.lanche.design.patterns;


import org.am.lanche.model.Pedido;
import org.am.lanche.model.Promocao;

public interface Desconto {
	
	public void setProximo(Desconto proximo);
	
	public void calcular(Pedido pedido, Promocao promocao);
	
}