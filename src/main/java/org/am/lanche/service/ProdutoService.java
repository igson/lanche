package org.am.lanche.service;

import java.util.List;
import java.util.Optional;

import org.am.lanche.iservice.IProdutoService;
import org.am.lanche.model.Produto;
import org.am.lanche.model.TipoProduto;
import org.am.lanche.repositories.IProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service("produtoService")
@Transactional(readOnly=true, propagation=Propagation.REQUIRED)
public class ProdutoService implements IProdutoService {

	
	@Autowired
	private IProdutoRepository produtoRepository;
	

	@Transactional(readOnly=true, propagation=Propagation.REQUIRED)
	public Produto buscaPorCodigo(String codigo) {
		return this.produtoRepository.findByCodigo(codigo);
	}
	
	@Transactional(readOnly=true, propagation=Propagation.REQUIRED)
	public List<Produto> listarTodos() {
		return this.produtoRepository.findAll();
	}
	
	@Transactional(readOnly=true, propagation=Propagation.REQUIRED)
	public List<Produto> listarPorCategoria(TipoProduto tipoProduto) {
		return this.produtoRepository.findByTipoProduto(tipoProduto);
	}
	
	
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	public Produto salvar(Produto produto) {
		return this.produtoRepository.save(produto);
	}


	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	public void remover(String codigo) {
		Produto produto = this.buscaPorCodigo(codigo);
		this.produtoRepository.delete(produto);
	}
	
	
	@Transactional(readOnly=false, propagation=Propagation.REQUIRED)
	public void remover(Integer id) {
		Optional<Produto> produto = this.produtoRepository.findById(id);
		this.produtoRepository.delete( produto.get() );
	}

}
