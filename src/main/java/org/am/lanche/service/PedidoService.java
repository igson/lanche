package org.am.lanche.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.am.lanche.design.patterns.CalculadoraDeDesconto;
import org.am.lanche.exception.NegocioException;
import org.am.lanche.iservice.IPedidoService;
import org.am.lanche.model.ItemPedido;
import org.am.lanche.model.Pedido;
import org.am.lanche.model.Promocao;
import org.am.lanche.model.SearchCriteria;
import org.am.lanche.model.SearchOperation;
import org.am.lanche.model.StatusPedido;
import org.am.lanche.repositories.IPedidoRepository;
import org.am.lanche.repositories.PedidoSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("pedidoService")
@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
public class PedidoService implements IPedidoService {

	private CalculadoraDeDesconto calculadoraDeDesconto = new CalculadoraDeDesconto();

	@Autowired
	private IPedidoRepository pedidoRepository;

	private Logger log = LogManager.getLogger("PEDIDO-LOG-SERVICE:");

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Pedido> listar(StatusPedido status) {

		if (status == StatusPedido.PENDENTE) {
			return this.pedidoRepository.findByStatusPedido(status);
		} else {
			return this.pedidoRepository.findAll();
		}

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void atualizar(Pedido pedidoResgatado, List<ItemPedido> novos) {

		pedidoResgatado.setValorTotal(new BigDecimal(0.0));

		//deve-se zerar o preço pois será recalculado com base nos novos itens adicionados
		zerarPrecoDosItens(pedidoResgatado);

		updateData(pedidoResgatado, novos);

		calculadoraDeDesconto.calculaDesconto(pedidoResgatado, Promocao.SEM_DESCONTO);

		System.out.println("Valor Total: " + pedidoResgatado.getValorTotal());

		this.pedidoRepository.save(pedidoResgatado);

	}

	
	private void zerarPrecoDosItens(Pedido pedido) {

		pedido.getItens().stream().map(temp -> {

			temp.setPreco(new BigDecimal(0.0));

			return temp;
		}

		).collect(Collectors.toList());

	}

	private void updateData(Pedido pedido, List<ItemPedido> itens) {

		for (ItemPedido item : itens) {
			pedido.adicionarItem(item);
		}

	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Pedido> listar(StatusPedido statusPedido, LocalDate dataEntregaPedido) {

		PedidoSpecification specStatus = new PedidoSpecification(
				new SearchCriteria("statusPedido", SearchOperation.EQUALITY, statusPedido));

		PedidoSpecification specDataEntregaPedido = new PedidoSpecification(
				new SearchCriteria("data", SearchOperation.EQUALITY, dataEntregaPedido));

		List<Pedido> pedidos = this.pedidoRepository
				.findAll(Specification.where(specStatus).and(specDataEntregaPedido));

		return pedidos;

	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public BigDecimal valorTotalVendaNoDia(LocalDate data) {
		return this.pedidoRepository.valorTotalPedidoDia(data);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Pedido buscaPorCodigo(String codigo) {
		return this.pedidoRepository.findByCodigo(codigo);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void realizarPagamento(String codigo) {

		Pedido pedido = this.buscaPorCodigo(codigo);
		pedido.setStatusPedido(StatusPedido.REALIZADO);

		this.pedidoRepository.save(pedido);

	}	

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Pedido salvar(Pedido pedido, Promocao promocao, List<ItemPedido> itens) {

		if (itens.size() == 0) {
			throw new NegocioException("É necessário ter ao menos 1 item no pedido.");
		}

		for ( ItemPedido itemPedido : itens ) {
			pedido.adicionarItem(itemPedido);
		}
		
		calculadoraDeDesconto.calculaDesconto(pedido, promocao);
		
		return this.pedidoRepository.save(pedido);

	}

}
