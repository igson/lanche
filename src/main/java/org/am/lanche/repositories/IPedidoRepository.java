package org.am.lanche.repositories;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.am.lanche.model.Pedido;
import org.am.lanche.model.StatusPedido;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;



@Repository("pedidoRepository")
public interface IPedidoRepository extends JpaRepository<Pedido, Integer>, JpaSpecificationExecutor<Pedido> {
	
	public List<Pedido> findByStatusPedido(StatusPedido statusPedido);
	
	public Pedido findByCodigo(@Param("codigo") String codigo);
	
	@Query("select sum(p.valorTotal) from Pedido p where data = :data and p.statusPedido = 'REALIZADO'")
	public BigDecimal valorTotalPedidoDia(@Param("data") LocalDate data);
	
}
