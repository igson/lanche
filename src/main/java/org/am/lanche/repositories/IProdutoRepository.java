package org.am.lanche.repositories;

import java.util.List;

import org.am.lanche.model.Produto;
import org.am.lanche.model.TipoProduto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository("produtoRepository")
public interface IProdutoRepository extends JpaRepository<Produto, Integer> {
	
	
	public Produto findByCodigo(@Param("codigo") String codigo);
	
	public List<Produto> findByTipoProduto(@Param("tipoProduto") TipoProduto tipoProduto);
	
}
