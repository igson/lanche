package org.am.lanche.repositories;

import java.util.Optional;

import javax.persistence.criteria.Predicate;


public interface QuerydslPredicateExecutor<T> {

	  Optional<T> findById(Predicate predicate);  

	  Iterable<T> findAll(Predicate predicate);   

	  long count(Predicate predicate);            

	  boolean exists(Predicate predicate);        


}